import {categoryConstants} from '../_constants';
import {categoryService} from '../_services';

export const categoryActions = {
    getAll,
    findById,
};

function getAll() {
    return dispatch => {
        dispatch(request());

        categoryService.getAll()
            .then(
                categories => dispatch(success(categories)),
                error => dispatch(failure(error))
            );
    };

    function request() {
        return {type: categoryConstants.GETALL_REQUEST}
    }

    function success(categories) {
        return {type: categoryConstants.GETALL_SUCCESS, categories}
    }

    function failure(error) {
        return {type: categoryConstants.GETALL_FAILURE, error}
    }
}

function findById(categoryId) {
    return dispatch => {
        dispatch(request());

        categoryService.findById(categoryId)
            .then(
                currentCategories => dispatch(success(currentCategories)),
                error => dispatch(failure(error))
            );
    };

    function request() {
        return {type: categoryConstants.FINDBYID_REQUEST}
    }

    function success(currentCategories) {
        return {type: categoryConstants.FINDBYID_SUCCESS, currentCategories}
    }

    function failure(error) {
        return {type: categoryConstants.FINDBYID_FAILURE, error}
    }
}