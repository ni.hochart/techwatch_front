import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { users } from './users.reducer';
import { posts } from './posts.reducer';
import { categories } from './categories.reducer';
import { alert } from './alert.reducer';

const rootReducer = combineReducers({
  authentication,
  users,
  alert,
  posts,
  categories
});

export default rootReducer;