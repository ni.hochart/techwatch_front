import {categoryConstants} from '../_constants';

export function categories(state = {}, action) {
    switch (action.type) {
        case categoryConstants.GETALL_REQUEST:
            return {
                loading: true
            };
        case categoryConstants.GETALL_SUCCESS:
            return {
                items: action.categories
            };
        case categoryConstants.GETALL_FAILURE:
            return {
                error: action.error
            };
        case categoryConstants.FINDBYID_REQUEST:
            return {
                loading: true
            };
        case categoryConstants.FINDBYID_SUCCESS:
            return {
                items: action.currentCategories
            };
        case categoryConstants.FINDBYID_FAILURE:
            return {
                error: action.error
            };
        default:
            return state
    }
}