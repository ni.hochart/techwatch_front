import React from 'react';
import {capitalize} from "../_helpers";
import {Link} from "react-router-dom";
import {connect} from "react-redux";

class WelcomeHeader extends React.Component {

    render() {
        let category;
        if (this.props.categories !== undefined && this.props.categories.items !== undefined) {
            category = this.props.categories.items
        }
        return (
            <section className="jumbotron text-center" style={{"marginBottom": 0}}>
                <div className="container">
                    <h1 className="jumbotron-heading">Hi {capitalize(this.props.user.username)}!</h1>
                    <p className="lead text-muted">Techwatch blog</p>
                    {category !== undefined && <p id={"categoryDetails"}>Category: {category.label}</p>}
                    {category !== undefined &&
                    <p>
                        <Link to="/" className="btn btn-primary my-2">Back to homepage</Link>
                    </p>
                    }
                    <p>
                        <Link to="/logout" className="btn btn-primary my-2">Logout</Link>
                    </p>
                </div>
            </section>
        );
    }
}

function mapStateToProps(state) {
    const {categories, showCategory, category} = state;
    return {
        categories,
        showCategory,
        category
    };
}

const connectedWelcomeHeader = connect(mapStateToProps)(WelcomeHeader);
export {connectedWelcomeHeader as WelcomeHeader};