import React from "react";
import {capitalize} from "../_helpers";
import {Link} from "react-router-dom";

export class Posts extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: props.posts,
            category: null
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            posts: nextProps.posts
        })
    }

    shouldComponentUpdate(nextProps) {
        return this.props.posts !== nextProps.posts;
    }

    render() {
        return (
            <div className="album py-5 bg-light">
                <div className="container">
                    {this.state.posts.loading && <em>Loading posts...</em>}
                    {this.state.posts.error && <span className="text-danger">ERROR: {this.state.posts.error}</span>}
                    {this.state.posts.items && this.state.posts.items.elements &&
                    <div className="row">
                        {this.state.posts.items.elements.map((post) =>
                            <div className={"col-md-3"} key={"post-card-" + post.id}>
                                <div className={"card mb-4 box-shadow"} key={post.id}>
                                    {post.meta !== null && post.meta !== undefined && post.meta.image !== "" &&
                                    <img className="card-img-top" style={{height: "150px"}} alt="website preview"
                                         id={"preview-post-" + post.id}
                                         src={post.meta.image}/>}

                                    {(post.meta === null || post.meta === undefined || post.meta.image === "") &&
                                    <img className="card-img-top" style={{height: "150px"}} alt="website preview"
                                         id={"preview-post-" + post.id}
                                         src="https://testament.co.uk/media/catalog/product/cache/1/small_image/300x/17f82f742ffe127f42dca9de82fb58b1/placeholder/default/no_image_available_300x300_1.jpg"/>}

                                    <div className={"card-body"}>

                                        <h4 className={"card-text"}><a style={{color: "black"}} href={post.url}
                                                                       className={"card-link"}>{capitalize(post.title)}</a>
                                        </h4>

                                        {post.meta !== undefined && post.meta !== null && post.meta.title !== "" &&
                                        <h5 className={"card-text"}><a style={{color: "black"}} href={post.url}
                                                                       className={"card-link"}>{post.meta.title}</a>
                                        </h5>}

                                        {post.meta !== undefined && post.meta !== null && post.meta.description !== "" &&
                                        <p className="card-text">{post.meta.description}</p>}
                                        {post.categories &&
                                        <div>
                                            {post.categories.map((category) =>
                                                <Link to={"category/" + category.id} className={"card-link"}
                                                      key={"category" + category.id + "_post" + post.id}>
                                                    #{category.label}
                                                </Link>
                                            )}
                                        </div>
                                        }
                                        <br/>
                                        <div className={"d-flex justify-content-between align-items-center"}>
                                            <div className="btn-group">
                                                <a target="_blank" href={post.url}>
                                                    <button type="button"
                                                            className="btn btn-sm btn-outline-secondary">View
                                                    </button>
                                                </a>
                                            </div>
                                            <small className="text-muted">{post.creation_date}</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                    }
                </div>
            </div>
        )
    }
}