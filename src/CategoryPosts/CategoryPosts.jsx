import React from 'react';
import {connect} from 'react-redux';
import {postActions} from "../_actions/post.actions";
import {WelcomeHeader, Posts} from "../_components";
import {categoryActions} from "../_actions";

class CategoryPostsPage extends React.Component {
    componentDidMount() {
        this.props.dispatch(postActions.getByCategory(this.props.match.params.id));
        this.props.dispatch(categoryActions.findById(this.props.match.params.id));
    }

    render() {
        const {user, posts, category} = this.props;
        return (
            <div>
                <WelcomeHeader user={user} showCategory={true} category={category}/>
                <Posts posts={posts}/>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const {posts, authentication} = state;
    const {user} = authentication;
    return {
        user,
        posts
    };
}

const connectedCategoryPasts = connect(mapStateToProps)(CategoryPostsPage);
export {connectedCategoryPasts as CategoryPosts};