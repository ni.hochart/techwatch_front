import React from 'react';
import {connect} from 'react-redux';
import {postActions} from "../_actions/post.actions";
import {WelcomeHeader, Posts} from "../_components";

class HomePage extends React.Component {
    componentDidMount() {
        this.props.dispatch(postActions.getAll());
        delete this.props.categories
    }

    render() {
        const {user, posts} = this.props;
        return (
            <div>
                <WelcomeHeader showCategory={false} user={user}/>
                <Posts posts={posts}/>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const {posts, authentication} = state;
    const {user} = authentication;
    return {
        user,
        posts
    };
}

const connectedHomePage = connect(mapStateToProps)(HomePage);
export {connectedHomePage as HomePage};