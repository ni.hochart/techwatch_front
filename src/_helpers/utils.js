export const capitalize = Capitalize;

/**
 * @return {string}
 */
function Capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}
